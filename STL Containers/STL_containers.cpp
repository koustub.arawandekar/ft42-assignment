#include <iostream>
#include <vector>
#include <list>
#include <array>
#include <stack>
#include <queue>
#include <deque>
#include <forward_list>
#include <set>
#include <map>
#include<iterator>
#include <unordered_set>
#include <unordered_map>
#include <string>
#include <algorithm>

using namespace std;

typedef unordered_multimap<string, int>::iterator unit;

void printunmp(unordered_multimap<string, int> unmp)
{
    unit it = unmp.begin();
    for (; it != unmp.end(); it++) {
        cout << "<" << it->first << ", " << it->second << ">  ";
        cout << endl;
    }
}

typedef unordered_multiset<int>::iterator umit;
void printunms(unordered_multiset<int> unms)
{
    umit it = unms.begin();
    for (; it != unms.end(); it++)
        cout << *it << " ";
    cout << endl;
}

int main()
{
    cout << "*****Array Container*****" << endl;
    array<int, 5> arcon1 = {4, 6, 2, 9, 7};
    array<int, 6> arcon2 = { 5, 20, 35, 40, 55, 80};
    cout << "\nSize of array1 :" << arcon1.size() << endl;
    cout << "Size of array2 :" << arcon2.size() << endl;

    sort(arcon1.begin(), arcon1.end());
    cout << "Sorted array1 : ";
    for (auto i : arcon1)
        cout << i << ' ';
    arcon2.fill(4);
    cout << "\nFilling array2 with 4 : ";
    for (auto i : arcon2)
        cout << i << ' ';

    cout << "\n\n\n\n*****Vector Container*****" << endl;
    vector<int> veccon;
    veccon.push_back(1);
    veccon.push_back(2);
    veccon.push_back(3);
    cout << "\nElements of vector: " << veccon[0] << " " << veccon[1] << " " << veccon[2] << endl;
    veccon[0] = 10;
    cout << "Modifying element of vector using operator: " << veccon[0] << " " << veccon[1] << " " << veccon[2] << endl;

    cout << "\n\n\n\n*****Deque Container*****" << endl;
    array<int, 7> decon1 = {4, 6, 2, 9, 7, 1, 12};
    array<int, 6> decon2 = { 5, 20, 35, 40, 55, 80};

    cout << "\nSize of array1 :" << decon1.size() << endl;
    cout << "Size of array2 :" << decon2.size() << endl;

    cout << "Initial array1 : ";
    for (auto i : decon1)
        cout << i << ' ';

    sort(decon1.begin(), decon1.end());
    cout << "\nAfter sorting array1 : ";
    for (auto i : decon1)
        cout << i << ' ';
    decon2.fill(5);
    cout << "\nFilled array2 : ";
    for (auto i : decon2)
        cout << i << ' ';

    cout << "\n\n\n\n*****Forward List Container*****" << endl;

    forward_list<int> foli1;
    forward_list<int> foli2;

    foli1.assign({ 2, 4, 8 });
    foli2.assign(2, 10);

    cout << "\nElements of first forward list are : ";
    for (int& a : foli1)
        cout << a << " ";
    cout << endl;

    cout << "Elements of second forward list are : ";
    for (int& b : foli2)
        cout << b << " ";
    cout << endl;

    cout << "\n\n\n\n*****List Container*****" << endl;
    list<int> lt;
    lt.push_back(3);
    lt.push_back(6);
    lt.push_back(9);
    lt.push_back(12);
    auto it = lt.begin();
    advance(it, 2);
    cout << "Accessing element of list at position 2: " << *it << endl;

    cout << "\n\n\n\n*****Set Container***** :" << endl;
    std::set<char> stc;
    stc.insert('K');
    stc.insert('D');
    stc.insert('A');
    std::cout << "Elements in set: ";
    for (auto& str : stc) {
        std::cout << str << ' ';
    }
    std::cout << '\n';

    cout << "\n\n\n\n*****Map Container*****" << endl;
    map<string, int> mpcon;
    mpcon["first"] = 18;
    cout << "Elements of map: " << mpcon["first"] << endl;
    mpcon["first"] = 21;
    cout << "Modifying elements of map: " << mpcon["first"] << endl;

    cout << "\n\n\n\n*****Multiset Container*****" << endl;

    multiset<int, greater<int> > mulscon1;
    mulscon1.insert(40);
    mulscon1.insert(30);
    mulscon1.insert(20);
    mulscon1.insert(50);
    mulscon1.insert(50);
    mulscon1.insert(10);

    // printing multiset gquiz1
    multiset<int, greater<int> >::iterator itr;
    cout << "\nThe multiset mulscon1 is : ";
    for (itr = mulscon1.begin(); itr != mulscon1.end(); ++itr) {
        cout << *itr << " ";
    }
    cout << endl;

    multiset<int> mulscon2(mulscon1.begin(), mulscon1.end());
    // print all elements of the multiset gquiz2
    cout << "\nmulscon2 after assigning elements of mulscon1 : ";
    for (itr = mulscon2.begin(); itr != mulscon2.end(); ++itr) {
        cout << *itr << " ";
    }
    cout << endl;

    cout << "\nmulscon2 after removal of elements less than 30 : ";
    mulscon2.erase(mulscon2.begin(), mulscon2.find(30));
    for (itr = mulscon2.begin(); itr != mulscon2.end(); ++itr) {
        cout << *itr << " ";
    }

    int num;
    num = mulscon2.erase(50);
    cout << "\n\nRemoving element/s '50' from mulscon2 : ";
    for (itr = mulscon2.begin(); itr != mulscon2.end(); ++itr) {
        cout << *itr << " ";
    }
    cout << endl;
    cout << "mulscon1.lower_bound(40) : " << *mulscon1.lower_bound(40) << endl;
    cout << "mulscon1.upper_bound(40) : " << *mulscon1.upper_bound(40) << endl;
    cout << "mulscon2.lower_bound(40) : " << *mulscon2.lower_bound(40) << endl;
    cout << "mulscon2.upper_bound(40) : " << *mulscon2.upper_bound(40) << endl;

    cout << "\n\n\n\n*****Multimap Container*****" << endl;
    {
        multimap<int, int> multmp1;

        multmp1.insert(pair<int, int>(1, 40));
        multmp1.insert(pair<int, int>(2, 30));
        multmp1.insert(pair<int, int>(3, 60));
        multmp1.insert(pair<int, int>(6, 50));
        multmp1.insert(pair<int, int>(6, 10));

        multimap<int, int>::iterator itr;
        cout << "\nMultimap multmp1 : \n";
        cout << "\tKEY\tELEMENT\n";
        for (itr = multmp1.begin(); itr != multmp1.end(); ++itr) {
            cout << '\t' << itr->first << '\t' << itr->second << '\n';
        }

        multmp1.insert(pair<int, int>(4, 50));
        multmp1.insert(pair<int, int>(5, 10));

        cout << "\nMultimap multmp1 after adding extra elements : \n";
        cout << "\tKEY\tELEMENT\n";
        for (itr = multmp1.begin(); itr != multmp1.end(); ++itr) {
            cout << '\t' << itr->first << '\t' << itr->second << '\n';
        }

        multimap<int, int> multmp2(multmp1.begin(), multmp1.end());
        cout << "\nMultimap multmp2 after assigning from multmp1 : \n";
        cout << "\tKEY\tELEMENT\n";
        for (itr = multmp2.begin(); itr != multmp2.end(); ++itr) {
            cout << '\t' << itr->first << '\t' << itr->second << '\n';
        }

        cout << "\ngquiz2 after removal of elements less than key=3 : \n";
        cout << "\tKEY\tELEMENT\n";
        multmp2.erase(multmp2.begin(), multmp2.find(3));
        for (itr = multmp2.begin(); itr != multmp2.end(); ++itr) {
            cout << '\t' << itr->first << '\t' << itr->second << '\n';
        }

        int num;
        num = multmp2.erase(4);
        cout << "After removing elements from ngquiz2 with key=4 : ";
        cout << "\tKEY\tELEMENT\n";
        for (itr = multmp2.begin(); itr != multmp2.end(); ++itr) {
            cout << '\t' << itr->first << '\t' << itr->second << '\n';
        }
        cout << endl;

        cout << "multmp1.lower_bound(5) : " << "\tKEY = ";
        cout << multmp1.lower_bound(5)->first << '\t';
        cout << "\tELEMENT = " << multmp1.lower_bound(5)->second << endl;
        cout << "multmp1.upper_bound(5) : " << "\tKEY = ";
        cout << multmp1.upper_bound(5)->first << '\t';
        cout << "\tELEMENT = " << multmp1.upper_bound(5)->second << endl;
    }

    cout << "\n\n\n\n*****Unordered-set Container*****" << endl;

    unordered_set<int> unodst {1, 100, 10, 70, 100};
    cout << "numbers = ";
    for(auto &num: unodst)
    {
        cout <<num<<" ";
    }

    cout << "\n\n\n\n*****Unordered-map Container*****" << endl;
    unordered_map<string, int> unodmp;
    unodmp["first"] = 10;
    unodmp["second"] = 20;
    unodmp["third"] = 33;
    cout << "\nElements of unordered_map: " << unodmp["first"] << endl;
    unodmp["first"] = 12;
    cout << "Modifying elements of unordered_map: " << unodmp["first"] << endl;

    cout << "\n\n\n\n*****Unordered-multiset Container*****" << endl;
    {
        unordered_multiset<int> unodms1;
        unordered_multiset<int> unodms2({ 1, 3, 1, 7, 2, 3, 4, 1, 6 });
        unodms1 = { 2, 7, 5, 0, 3, 7};

        if (unodms1.empty())
            cout << "\nUnordered multiset 1 is empty\n";
        else
            cout << "\nUnordered multiset 1 is not empty\n";

        cout << "\nSize of unordered multiset 2 is : " << unodms2.size() << endl;
        cout << "\nUnordered multiset 1 : ";
        printunms(unodms1);
        unodms1.insert(8);
        cout << "\nUnordered multiset 1 after inserting an element : ";
        printunms(unodms1);

        int val = 3;
        if (unodms1.find(val) != unodms1.end())
            cout << "\nUnordered multiset 1 contains " << val << endl;
        else
            cout << "\nUnordered multiset 1 does not contains " << val << endl;

        val = 5;
        int cnt = unodms1.count(val);
        cout << "\n" << val << " appears " << cnt << " times in unordered multiset 1 \n";
        val = 9;
        if (unodms1.count(val))
            cout << "\nUnordered multiset 1 contains " << val << endl;
        else
            cout << "\nUnordered multiset 1 does not contains " << val << endl;
        val = 1;
        pair<umit, umit> erange_it = unodms2.equal_range(val);
        if (erange_it.first != erange_it.second)
            cout << "\n" << val << " appeared atleast once in unordered_multiset \n";
        printunms(unodms2);

        unodms2.erase(val);
        printunms(unodms2);
        unodms1.clear();
        unodms2.clear();

        if (unodms1.empty())
            cout << "\nUnordered multiset 1 is empty\n";
        else
            cout << "\nUnordered multiset 1 is not empty\n";
    }

    cout << "\n\n\n\n*****Unordered-multimap Container*****" << endl;
    {
        unordered_multimap<string, int> unodmm1;
        unordered_multimap<string, int> unodmm2(
            { { "apple", 1 },
              { "ball", 2 },
              { "apple", 10 },
              { "cat", 7 },
              { "dog", 9 },
              { "cat", 6 },
              { "apple", 1 } });

        unodmm1 = unodmm2;
        cout << "\nUnordered-multimap unodmm1: " ;
        printunmp(unodmm1);
        if (unodmm2.empty())
            cout << "\nUnordered multimap 2 is empty\n";
        else
            cout << "\nUnordered multimap 2 is not empty\n";
        cout << "\nSize of unordered multimap 1 is " << unodmm1.size() << endl;

        string key = "apple";
        unit it = unodmm1.find(key);
        if (it != unodmm1.end()) {
            cout << "\nkey " << key << " is there in unordered multimap 1\n";
            cout << "\none of the value associated with " << key << " is " << it->second << endl;
        }
        else
            cout << "\nkey " << key << " is not there in unordered multimap 1\n";

        int cnt = unodmm1.count(key);
        cout << "\ntotal values associated with " << key << " are " << cnt << "\n\n";
        cout << "\nUnordered-multimap unodmm2: " ;
        printunmp(unodmm2);

        unodmm2.insert(make_pair("dog", 11));
        unodmm2.insert({ { "alpha", 12 }, { "beta", 33 } });
        cout << "\nAfter insertion of <alpha, 12> and <beta, 33> : \n";
        printunmp(unodmm2);

        unodmm2.erase("apple");
        cout << "\nAfter deletion of apple : \n";
        printunmp(unodmm2);

        unodmm2.clear();
        if (unodmm2.empty())
            cout << "\nUnordered multimap 2 is empty\n";
        else
            cout << "\nUnordered multimap 2 is not empty\n";
    }

    cout << "\n\n\n\n*****Stack Container*****" << endl;
    stack<int> stk;
    stk.push(1);
    stk.push(2);
    stk.push(3);
    cout << "\nAccessing element of stack: " << stk.top() << endl;
    stk.pop();
    cout << "\nModifying element of stack: " << stk.top() << endl;

    cout << "\n\n\n\n*****Queue Container*****" << endl;
    queue<int> que;
    que.push(1);
    que.push(2);
    que.push(3);
    cout << "\nAccessing element of queue: " << que.front() << endl;
    que.pop();
    cout << "\nModifying element of queue: " << que.front() << endl;

    cout << "\n\n\n\n*****Priority-Queue Container*****" << endl;
    {
        int arr[6] = { 10, 2, 4, 8, 6, 9 };
        priority_queue<int> prque;
        cout << "\nArray: ";
        for (auto i : arr) {
            cout << i << ' ';
        }
        cout << endl;

        for (int i = 0; i < 6; i++) {
            prque.push(arr[i]);
        }

        cout << "\nPriority Queue: ";
        while (!prque.empty()) {
            cout << prque.top() << ' ';
            prque.pop();
        }
    }
    return 0;
}
